<?php

namespace DataFixtures;

use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Doctrine\Repository\TrickRepository;
use App\Domain\Trick\Manager\TrickImageManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class TrickImageFixtures extends Fixture implements DependentFixtureInterface
{

    private $imageFixtureDir;

    /**
     * @var TrickRepository
     */
    private $trickRepository;

    /**
     * @var TrickImageManager
     */
    private $trickImageManager;

    private $uploadDir;

    private $fileSystem;

    public function __construct($imageFixtureDir, $uploadDir, TrickRepository $trickRepository,
                                TrickImageManager $trickImageManager)
    {
        $this->imageFixtureDir = $imageFixtureDir;
        $this->trickRepository = $trickRepository;
        $this->trickImageManager = $trickImageManager;
        $this->uploadDir = $uploadDir;
        $this->fileSystem = new Filesystem();
    }

    /**
     * @param ObjectManager $objectManager
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE trick_image AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $this->fileSystem->remove($this->uploadDir);

        for ($i = 0; $i < 40; $i++) {
            $this->addImage();
        }
    }

    public function getDependencies()
    {
        return [
            TrickFixtures::class
        ];
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function addImage(): void
    {
        $imgName = rand(1, 10);
        $this->fileSystem->copy($this->imageFixtureDir . '/' . $imgName . '.jpg', $this->imageFixtureDir . '/' . $imgName . '-cp.jpg');
        $image = new File($this->imageFixtureDir . '/' . $imgName . '-cp.jpg', $imgName . '-cp.jpg');

        /** @var Trick $trick */
        $trick = $this->trickRepository->findOneBy(['id' => rand(1, 16)]);

        $this->trickImageManager->addImagesFromTrickType($trick, [$image]);

        $this->trickRepository->save($trick);
    }
}