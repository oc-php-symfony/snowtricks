<?php

namespace DataFixtures;

use App\Domain\Trick\Doctrine\Entity\Slide;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SlideFixtures extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE slide AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $slides = ['50-50','Bluntslide','Boardslide','Lipslide','Noseblunt','Noseslide'];

        foreach ($slides as $slideName) {
            $slide = new Slide();
            $slide->setName($slideName);

            $objectManager->persist($slide);

        }

        $objectManager->flush();
    }
}
