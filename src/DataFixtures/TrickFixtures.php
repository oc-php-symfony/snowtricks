<?php

namespace DataFixtures;

use App\Common\Utils\TextUtils;
use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Doctrine\Repository\GrabRepository;
use App\Domain\Trick\Doctrine\Repository\SlideRepository;
use App\Domain\Trick\Doctrine\Repository\VariantRepository;
use App\Domain\User\Doctrine\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TrickFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var SlideRepository
     */
    private $slideRepository;

    /**
     * @var GrabRepository
     */
    private $grabRepository;

    /**
     * @var VariantRepository
     */
    private $variantRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(SlideRepository $slideRepository, GrabRepository $grabRepository,
                                    VariantRepository $variantRepository, UserRepository $userRepository)
    {
        $this->slideRepository = $slideRepository;
        $this->grabRepository = $grabRepository;
        $this->variantRepository = $variantRepository;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE trick AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $tricks = [
            [
                'name' => 'Better Resort Runs: Season Equipment Aero Snowboard Review',
                'description' => 'The Season Equipment Aero snowboard excels at hardpacked groomer runs. By the end of the day, I was riding better than I had in years.
As I glided off the lift and strapped into the Season Equipment Aero snowboard, I was giddy with excitement. It’s not every day I test gear designed by a pro snowboarder who’s also my friend.
Austin Smith co-founded Season Equipment, a new ski and snowboard company, with Eric Pollard, a professional freeride skier, artist, and filmmaker (check out his latest amazing film here).
Smith, who has been riding since he was 10, is in charge of all things boards. And Pollard, who turned pro when he was 15, is in charge of all things skiing.
I took the new Season Equipment Aero snowboard ($499) out on some groomers to see how it performs. Short answer? I’m stoked on it.
Season Equipment Gear: A Perfect Team
Season Equipment offers three options: the Aero, Nexus, and Forma. Not only do these span many types of skiing and riding, but they even come in one- and two-plank varieties. I love the idea of going out with my husband, who’s a skier, and having a similar experience even though I’m on a board.',
            ],
            [
                'name' => 'As a rider with 20-plus years of riding under my belt',
                'description' => 'Aiming for Sustainability
What impressed me most, however, was the fact that the brand’s equipment comes with full service. Wait, what? Yes, that was my reaction as well. Every plank comes with unlimited complimentary machine wax, a yearly tune-up, and 30% off additional repair services.
But it does come with a catch: You have to live near an evo shop to get full service. Season Equipment’s goal is to create a product that lasts forever. There will be no “new” color schemes or designs every season to make people crave the newest, latest piece of equipment. Instead, Smith and Pollard aim to create a product that spans decades.
“Don’t tell me how you like it now. Let me know in 2040 — then I will know how we did,” Smith said.
First Look: Season Equipment Aero Snowboard',
            ],
            [
                'name' => 'A Perfect Team',
                'description' => 'And honestly, after spending the last few winters in New Zealand, I thought my skill level had hit a permanent plateau. However, after riding the Aero, my fire has been stoked — and the riding flame is high.
According to Season, the Aero board was made for resort riding, as it excels at hardpacked groomer runs. I could tell this instantly when I headed out.
While it performed great in the deep powder, as soon as I hit the freshly groomed runs, I could feel the difference in its performance. Going from edge to edge felt like a beautiful, effortless dance. And the more time I got on the board, the more confidence I gained.',
            ],
            [
                'name' => 'By the end of the day',
                'description' => 'Season Equipment Gear Specs Charting Success
If you look on the brand’s website, all the specs for each board and ski are displayed in comprehensive, easily digestible charts. There’s also a whole section that compares all the boards and skis to each other, with both diagrams and charts.
As a gear reviewer, user, and visual learner, I enjoyed this. At a glance, I learned a lot about each board and what made each different. I could tell some major time and effort went into it.',
            ],
            [
                'name' => 'Size Range & Inclusivity',
                'description' => 'As a small human (5’1″ and 110 pounds), I love the size range of the boards. They go down to a 145 and up to a 160. But what I appreciate the most is the gender inclusivity.
Just like Season’s ethos on finding and highlighting the common ground of snowboarders and skiers, the brand isn’t making gender-specific equipment. Although I do like my women’s-specific gear in other sports, I do see this as an area where division isn’t necessary.
Overall, at the end of the day, I was more than pleased with the Aero board and all the places I could go on it. It reinvigorated my love for snowboarding and left me with hope for outdoor brands. This isn’t just another new ski and snowboard company. It’s one that is hoping to change the outdoor consumer world as we know it.',
            ],
            [
                'name' => 'Check Price at Season EquipmentCheck',
                'description' => 'The 10 Best Snowboard Boots of 2021
Whether heading into the backcountry or looking for an all-mountain winner, we found the best snowboard boots of 2021. Read more…
The Best Snowboard Bindings of 2021
Bindings are a vital piece of gear that can make or break your time on the mountain. We\'ve got the best of the year lined up and ready to shred.  Read more…',
            ],
            [
                'name' => 'Ride Like a Pro With Signal\'s Snowboard Membership Program',
                'description' => 'Founded in 2004 as a small, rider-owned brand, Signal Snowboards is one of the first snowboard companies to offer memberships with a monthly payment option for buying snowboards. Signal Snowboards aims to provide every rider with high-quality treatment and snowboards. Regardless of skill level, Signal connects and guides its members through the best experience in snowboarding.
After a member joins, the adventure begins with snowboards designed by pro riders and top engineers delivered right to their door. To keep members riding in style, Signal also offers Signature Gift Boxes filled with hand-picked items that will arrive throughout the season.t',
            ],
            [
                'name' => 'Pro Membership also includes Signal Care',
                'description' => 'LAS VEGAS (FOX5) -- The owner of Sin City Snowboards had to run a little errand recently, and close his store for a couple days, in part because of all the new snow on the ground.
"Recent, snow definitely brought more people to snowboard. It always does," said store owner Glenn Roberts.
Roberts had to close to restock his store, which includes new and used snowboards. He said it\'s the fifth time this year he has restocked. Last year he only restocked once.',
            ],
            [
                'name' => 'Snow storm leads to good business for Las Vegas snowboard shop',
                'description' => 'Viviane Segura
He said along with the new snow, the COVID-19 pandemic comes  into play. He said suppliers were shut down for a few months, making it tough to get product.
During his recent trip to Salt Lake City, he was able to only buy ten sets of skis and 34 snowboards -- all the product his suppliers had.',
            ],
            [
                'name' => 'Snow in Red Rock Canyon on Jan. 25, 2021. (Courtesy)',
                'description' => '"I\'d buy a thousand snowboards right now if I could," said Roberts.
Roberts is reopening his store at Eastern and Reno avenues on Thursday but believes he will sell his supply of snowboards by the end of February.',
            ],
            [
                'name' => 'Learn to ski in the Alps: the 5 best ski resorts for beginners',
                'description' => 'Skiing is a pleasure! You don’t have to be an expert in skiing to fully enjoy the slopes. Admittedly, learning how to ski can be challenging, but it’s worthwhile. There are ski resorts that are especially suited for beginners who are eager to learn to ski. These ski resorts have a variety of gently,  wide slopes, training areas, numerous practice lifts and rather easy valley runs. Good ski schools, as well as private instructors also contribute to an overall enjoyable experience on the slopes for beginners.
The CheckYeti team has visited numerous ski resorts in the Alps and presents you with the top 5 ski resorts for beginners. These resorts provide you with a beginner-friendly environment'
            ],
            [
                'name' => 'Méribel, France: a paradise for beginners amidst the French Alps',
                'description' => 'Méribel is one of the four main ski resorts of the ski alliance Les 3 Vallées, the largest ski resort in Europe. The town is located at an altitude of roughly 1500m in the east of France, really close to the Italian border. It can be reached by car in around 2h from Geneva and 1.5h from Grenoble.
There are several reasons why Méribel is one of the best places for beginners to learn to ski: due to its location in the middle of Les 3 Vallées and the fact that over 50 % of the pistes are blue (easy) or green (very easy), almost all of the ski resort is accessible for beginners. Also, there are fast tracks for beginners'
            ],
            [
                'name' => 'Zermatt, Switzerland: start high, aim higher',
                'description' => 'Zermatt is the highest and one of the sunniest ski resorts in Switzerland. It can be reached from Bern in approx. 2.5h. Since the town is car-free, visitors have to take the local train, the Glacier Express, for the final part of the journey. The town is located at the foot of the majestic Matterhorn (4478m) and is surrounded by numerous peaks of 4000m or more. The views from the pistes and the sun terraces are stunning. Taking a well-deserved break from skiing practice in Zermatt is an experience in itself.
There is an abundance of opportunities for beginners in this ski resort. Several ski schools offer a broad selection of beginners’ courses for every age to learn to ski. 75km of blue pistes, special slopes for beginners, a small training lift directly in town as well as several practice areas, including the Snowli Kids Village, are just some of the features that make Zermatt one of the best ski resorts for beginners.'
            ],
            [
                'name' => 'Zell am See, Austria: down to earth and versatile',
                'description' => 'The charming and lively town of Zell am See is beautifully located at the shore of Lake Zeller in the Austrian state of Salzburg. It can be reached by car in about 1.5h from Salzburg City and 2h from Munich or Innsbruck. There are also regular trains connecting Zell am See to Salzburg and Innsbruck.
Together with some of its neighbouring ski resorts, the town forms part of the ski region Zell am See – Kaprun. Schmittenhöhe is the area closest to the town centre. Although it is rather small, there are plenty of pistes for beginners. Several of the blue pistes are located next to the cityXpress cable car and can thus be reached comfortably.'
            ],
            [
                'name' => 'Tignes, France: rich in snow and natural beauty',
                'description' => 'Together with the well-known town of Val d’Isère, Tignes forms the large and beginner-friendly ski resort Espace Killy. The modern and rather luxurious village lies at the shore of Lac de Tignes at an altitude of 2000m and is consequently one of the highest villages and one of the ski resorts with the most reliable snow conditions in Europe. It can be reached in around 3h from Lyon or Geneva.
Situated in the beautiful Parc national de la Vanoise, the ski resort Tignes offers perfect conditions for beginners. 57 % of the slopes with a total length of 170km are either marked in blue or green. Several free practice lifts and lots of activities designed for families complement this offer. Last but not least, the ski schools in Tignes are very internationally-minded and offer their courses in numerous languages, including English, Spanish, Dutch, Italian and Russian.'
            ],
            [
                'name' => 'St Anton am Arlberg, Austria: fun times at the “cradle of alpine skiing”',
                'description' => 'St Anton is a village and ski resort at the border between Tyrol and Vorarlberg in the west of Austria. It is part of the Ski Arlberg skiing region, which also includes the famous and luxurious ski resort of Lech. St Anton is a lot more down-to-earth than Lech and is renowned for its après-ski bars and parties. It can be reached by car in around 1h from Innsbruck. The train station is only 5min away from the slopes and even international trains, coming via Zurich or Innsbruck/Salzburg, stop here regularly.
St Anton has been a popular ski resort amongst beginners since the first ski school in Austria opened here in 1921. 132km of blue pistes, many of which are really close to the town centre, serve as a perfect training ground for beginners. Most of the practice slopes are connected, which makes moving from one hill to another quite easy. A special beginners’ ticket, which is cheaper than the normal lift pass, is available and valid for the 4 practice lifts in St Anton.'
            ]
        ];
        
        $trickRands = [
            'is_switch' => rand(0, 1) === 0,
            'rotation' => rand(0, 16) * 90,
            'direction' => Trick::TRICK_DIRECTIONS[rand(0, 2)],
            'slideId' => rand(0, 5),
            'grabId' => rand(0, 5),
            'variantId' => rand(0, 7),
            'userId' => 'user' . rand(0, 9)
        ];

        foreach ($tricks as $trickArray) {

            $trick = new Trick();
            $trick = ($trick)
                ->setName($trickArray['name'])
                ->setSlug(TextUtils::slugify($trickArray['name']))
                ->setDescription($trickArray['description'])
                ->setIsSwitch($trickRands['is_switch'])
                ->setRotation($trickRands['rotation'])
                ->setDirection($trickRands['direction'])
                ->setSlide($this->slideRepository->findOneBy(['id' => $trickRands['slideId']]))
                ->setGrab($this->grabRepository->findOneBy(['id' => $trickRands['grabId']]))
                ->setVariant($this->variantRepository->find(['id' => $trickRands['variantId']]))
                ->setUser($this->userRepository->findOneBy(['name' => $trickRands['userId']]))
            ;

            $objectManager->persist($trick);
        }

        $objectManager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            GrabFixtures::class,
            VariantFixtures::class,
            SlideFixtures::Class
        ];
    }
}