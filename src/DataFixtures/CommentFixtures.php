<?php

namespace DataFixtures;

use App\Domain\Comment\Doctrine\Entity\Comment;
use App\Domain\Trick\Doctrine\Repository\TrickRepository;
use App\Domain\User\Doctrine\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TrickRepository
     */
    private $trickRepository;

    public function __construct(UserRepository $userRepository, TrickRepository $trickRepository)
    {
        $this->userRepository = $userRepository;
        $this->trickRepository = $trickRepository;
    }

    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE comment AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $commentContents = [
            'Playfulness, fold, experience, icons – sublime dude',
            'Lavender. Flat design is going to die.',
            'This shot has navigated right into my heart.',
            'These are graceful and appealing mate',
            'Such magnificent.',
            'I\'d love to see a video of how it works.',
            'Such notification, many background, so fun',
            'Whoa.',
            'Mission accomplished. It\'s slick :-)',
            'Good. So good.',
            'Nice use of turquoise in this texture.',
            'I approve your animation!!',
            'Neat concept :-)',
            'Incredibly sleek shot mate',
            'It\'s excellent not just strong!',
            'Fabulous work you have here.',
            'Let me take a nap... great type, anyway.',
            'This is splendid work!!',
            'My 39 year old sister rates this style very appealing mate',
            'I want to learn this kind of colour palette! Teach me.',
        ];

        for ($i = 0; $i < 60; $i++) {
            $comment = $this->createComment($commentContents[rand(0, 19)], rand(1, 16), rand(1, 10));
            $objectManager->persist($comment);
        }

        $objectManager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TrickFixtures::class
        ];
    }

    private function createComment($content, $trickId, $userId)
    {
        $comment = new Comment();

        $comment = ($comment)
            ->setContent($content)
            ->setUser($this->userRepository->findOneBy(['id' => $userId]))
            ->setTrick($this->trickRepository->findOneBy(['id' => $trickId]))
        ;

        return $comment;
    }
}