<?php

namespace App\Infrastructure\Mailer\Mailjet\Form\Command;

class MailjetCommand
{
    public $subject;

    /** @var MailjetContactCommand $from */
    public $from;

    /** @var MailjetContactCommand $to */
    public $dest;

    /** @var MailjetContactCommand $cc */
    public $carbonCopy;

    /** @var MailjetContactCommand $bcc */
    public $bcc;

    public $templateId;

    /**
     * Array of 0-N MailjetVariableCommand
     *
     * @var array
     */
    public $vars = [];

    /**
     * Array o 0-N MailjetAttachmentCommand
     *
     * @var array
     */
    public $attachments = [];

    /** @var MailjetAttachmentCommand $attachment */
    public $attachment;


    public function getBody()
    {
        $body =  [
            'FromEmail' => $this->from->email,
            'FromName' => $this->from->name,
            'Subject' => $this->subject,
            'Mj-TemplateID' => $this->templateId,
            'Mj-TemplateLanguage' => true,
            'To' => $this->dest->name ? $this->dest->name.' <'.$this->dest->email.'>' : $this->dest->email,
            'MJ-TemplateErrorReporting' => 'damien.adam@heureetcontrole.fr',
        ];

        if ($this->bcc) {
            $body['Bcc'] = $this->bcc->name.' <'.$this->bcc->email.'>';
        }

        if (!empty($this->vars)) {
            /** @var MailjetVariableCommand $variable */
            foreach ($this->vars as $variable) {
                $body['Vars'][$variable->key] = $variable->value;
            }
        }

        if (!empty($this->attachments)) {
            /** @var MailjetAttachmentCommand $attachment */
            foreach ($this->attachments as $attachment) {
                $bodyAttachment = [];
                $bodyAttachment['Content-type'] = $attachment->contentType;
                $bodyAttachment['Filename'] = $attachment->fileName;
                $bodyAttachment['content'] = $attachment->content;

                $body['Attachments'][] = $bodyAttachment;
            }
        }

        return $body;
    }
}