<?php

namespace App\Domain\Trick\Controller;

use App\Common\Utils\TextUtils;
use App\Domain\Comment\Doctrine\Entity\Comment;
use App\Domain\Comment\Form\Type\CommentType;
use App\Domain\Comment\Manager\CommentManager;
use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Form\Type\TrickType;
use App\Domain\Trick\Manager\TrickImageManager;
use App\Domain\Trick\Manager\TrickManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Exception\NotImplementedException;

class TrickController extends AbstractController
{
    /**
     * @return Response
     */
    public function home()
    {
        return $this->render('home.html.twig');
    }

    /**
     * @param int $page
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function trickLoad(int $page = 1)
    {
        /** @var TrickManager $trickManager */
        $trickManager = $this->get('app.trick.manager');
        $tricks = $trickManager->findPaginated($page);

        $totaltricks = intval($trickManager->countAll());
        $showed = intval($this->container->getParameter('app.trick.comment_per_page')) * $page;
        $hasMoreResults = $totaltricks > $showed;

        return $this->render('Trick/grid.html.twig', [
            'tricks' => $tricks,
            'hasMoreResults' => $hasMoreResults
        ]);
    }

    /**
     * @return Response
     */
    public function index()
    {
        return $this->render('Trick/index.html.twig');
    }

    /**
     * @param Request $request
     * @param Trick $trick
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function show(Request $request, Trick $trick)
    {
        $form = $this->createForm(CommentType::class, $comment = new Comment());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

            /** @var CommentManager $commentManager */
            $commentManager = $this->get('app.comment.manager');
            $commentManager->save($comment, $trick, $this->getUser());
            $this->addFlash('success','Your comment has been created');

            return $this->redirectToRoute('trick_show', ['slug' => $trick->getSlug()]);
        }

        return $this->render('Trick/show.html.twig', [
            'commentForm' => $form->createView(),
            'trick' => $trick
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $form = $this->createForm(TrickType::class, $trick = new Trick());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var TrickManager $trickManager */
            $trickManager = $this->get('app.trick.manager');

            /** @var TrickImageManager $trickImageManager */
            $trickImageManager = $this->get('app.trick_image.manager');
            $trickImageManager->addImagesFromTrickType($trick, $form->get('trickImages')->getData());

            $trick->setSlug(TextUtils::slugify($trick->getName()));
            $trick->setUser($this->getUser());

            $trickManager->save($trick);
            $this->addFlash('success', sprintf('%s trick has been created', $trick->getName()));

            return $this->redirectToRoute('trick_index');
        }

        return $this->render('Trick/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Trick $trick
     * @return RedirectResponse
     */
    public function delete(Trick $trick)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        try {
            /** @var TrickManager $trickManager */
            $trickManager = $this->get('app.trick.manager');
            $trickManager->delete($trick);
            $this->addFlash('success', sprintf('%s supprimé avec succès', $trick->getName()));

            return $this->redirectToRoute('trick_index');
        } catch (Exception $exception) {
            $this->addFlash('danger', sprintf('Erreur lors de la suppression de %s', $trick->getName()));

            return $this->redirectToRoute('trick_show', [
                'slug' => $trick->getSlug()
            ]);
        }
    }

    /**
     * @param Request $request
     * @param Trick $trick
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function edit(Request $request, Trick $trick)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $form = $this->createForm(TrickType::class, $trick);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var TrickManager $trickManager */
            $trickManager = $this->get('app.trick.manager');

            /** @var TrickImageManager $trickImageManager */
            $trickImageManager = $this->get('app.trick_image.manager');
            $trickImageManager->addImagesFromTrickType($trick, $form->get('trickImages')->getData());

            $trick->setSlug(TextUtils::slugify($trick->getName()));

            $trickManager->save($trick);
            $this->addFlash('success', sprintf('%s trick has been updated', $trick->getName()));

            return $this->redirectToRoute('trick_index');
        }

        return $this->render('Trick/create.html.twig', [
            'form' => $form->createView(),
            'trick' => $trick
        ]);
    }
}