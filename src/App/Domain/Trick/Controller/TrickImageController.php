<?php

namespace App\Domain\Trick\Controller;

use App\Domain\Trick\Doctrine\Entity\TrickImage;
use App\Domain\Trick\Manager\TrickImageManager;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TrickImageController extends AbstractController
{
    /**
     * @param Request $request
     * @param TrickImage $trickImage
     * @return JsonResponse
     * @throws ORMException
     */
    public function delete(Request $request, TrickImage $trickImage)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid('delete'.$trickImage->getId(), $data['_token'])) {
            /** @var TrickImageManager $trickImageManager */
            $trickImageManager = $this->get('app.trick_image.manager');
            $trickImageManager->delete($trickImage);

            return new JsonResponse(['success' => 1]);
        }

        return new JsonResponse(['error' => 'invalid token'], 400);
    }
}