<?php

namespace App\Domain\Trick\Doctrine\Entity;

use Doctrine\Common\Collections\Collection;

class Slide
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection
     */
    private $tricks;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Slide
     */
    public function setName(string $name): Slide
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add trick.
     *
     * @param Trick $trick
     *
     * @return Slide
     */
    public function addTrick(Trick $trick): Slide
    {
        $this->tricks[] = $trick;

        return $this;
    }

    /**
     * Remove trick.
     *
     * @param Trick $trick
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrick(Trick $trick)
    {
        return $this->tricks->removeElement($trick);
    }

    /**
     * Get tricks.
     *
     * @return Collection
     */
    public function getTricks()
    {
        return $this->tricks;
    }
}