<?php

namespace App\Domain\Trick\Doctrine\Repository;

use App\Common\Doctrine\Repository\BaseRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class TrickRepository extends BaseRepository
{
    /**
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAll()
    {
        $queryBuilder = $this->createQueryBuilder('t');
        $queryBuilder->select($queryBuilder->expr()->count('t.id'));
        $query = $queryBuilder->getQuery();

        return $query->getSingleScalarResult();
    }

    public function findPaginated(int $maxResults, int $page)
    {
        $firstResult = ($page - 1) * $maxResults;
        $qb = $this->createQueryBuilder('t');
        $qb->setFirstResult($firstResult);
        $qb->setMaxResults($maxResults);
        $qb->orderBy('t.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }
}