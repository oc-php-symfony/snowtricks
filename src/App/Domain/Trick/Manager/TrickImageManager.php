<?php

namespace App\Domain\Trick\Manager;

use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Doctrine\Entity\TrickImage;
use App\Domain\Trick\Doctrine\Repository\TrickImageRepository;
use Doctrine\ORM\ORMException;

class TrickImageManager
{
    /**
     * @var string
     */
    private $uploadPath;

    /**
     * @var TrickImageRepository
     */
    private $trickImageRepository;

    public function __construct(TrickImageRepository $trickImageRepository, string $uploadPath)
    {
        $this->trickImageRepository = $trickImageRepository;
        $this->uploadPath = $uploadPath;
    }

    public function addImagesFromTrickType(Trick $trick, array $images)
    {
        foreach ($images as $image) {
            $fileName = md5(uniqid()) . '.' .  $image->guessExtension();
            $image->move($this->uploadPath, $fileName);

            /** @var TrickImage $trickImage */
            $trickImage = new TrickImage();
            $trickImage->setFileName($fileName);
            $trick->addTrickImage($trickImage);
        }
    }

    /**
     * @param TrickImage $trickImage
     * @throws ORMException
     */
    public function delete(TrickImage $trickImage)
    {
        $this->unlinkImage($trickImage->getFileName());
        $this->trickImageRepository->removeAndFlush($trickImage);
    }

    public function unlinkImage($filename)
    {
        unlink($this->uploadPath. '/' . $filename);
    }
}