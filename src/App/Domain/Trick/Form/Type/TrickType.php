<?php

namespace App\Domain\Trick\Form\Type;

use App\Domain\Trick\Doctrine\Entity\Grab;
use App\Domain\Trick\Doctrine\Entity\Slide;
use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Doctrine\Entity\Variant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrickType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, [
                'label' => 'trick name'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'description',
                'attr' => [
                    'rows' => 5
                ]
            ])
            ->add('isSwitch', CheckboxType::class, [
                'label' => 'switch stance'
            ])
            ->add('rotation', RangeType::class, [
                'label' => 'rotation',
                'data' => $builder->getData()->getRotation() ?: 0,
                'attr' => [
                    'min' => 0,
                    'max' => 3240,
                    'step' => 90,
                    'oninput' => "updateTextInput(this.value);"
                ]
            ])
            ->add('direction', ChoiceType::class, [
                'expanded' => true,
                'multiple' => false,
                'choices'  => Trick::TRICK_DIRECTIONS,
                'choice_label' => function ($choice, $key, $value) {
                    return $value;
                },
                'data' => $builder->getData()->getDirection() ?: 'none',
                'attr' => [
                    'class' => 'form-check'
                ]
            ])
            ->add('grab', EntityType::class, [
                'placeholder' => 'Select a grab',
                'label' => 'grab',
                'required' => false,
                'class' => Grab::class,
                'choice_label' => 'name'
            ])
            ->add('slide', EntityType::class, [
                'placeholder' => 'Select a slide',
                'label' => 'slide',
                'required' => false,
                'class' => Slide::class,
                'choice_label' => 'name'
            ])
            ->add('variant', EntityType::class, [
                'placeholder' => 'Select a variant',
                'label' => 'variant',
                'required' => false,
                'class' => Variant::class,
                'choice_label' => 'name'
            ])
            ->add('trickImages', FileType::class, [
                'label' => 'add images',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'onchange' => "handleFiles(files);",
                    'accept' => "image/*"
                ]

            ])
            ->add('trickVideos', CollectionType::class, [
                'entry_type' => TrickVideoType::class,
                'label' => false,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trick::class
        ]);
    }
}