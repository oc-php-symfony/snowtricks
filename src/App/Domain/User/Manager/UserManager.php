<?php

namespace App\Domain\User\Manager;

use App\Common\Utils\TextUtils;
use App\Domain\User\Doctrine\Entity\User;
use App\Domain\User\Doctrine\Repository\UserRepository;
use App\Domain\User\Form\Command\PasswordResetCommand;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class UserManager
 * @package App\Domain\User\Manager
 */
class UserManager
{

    const PWD_LENGTH = 27;
    const VALIDATION_TOKEN_LENGTH = 25;
    const RST_TOKEN_LENGTH = 35;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * UserManager constructor.
     *
     * @param UserRepository $userRepository
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(UserRepository $userRepository, EncoderFactoryInterface $encoderFactory)
    {
        $this->userRepository = $userRepository;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signup(User $user)
    {
        $user->setValidationToken(TextUtils::generateRandomString(self::VALIDATION_TOKEN_LENGTH));

        $this->encodeAndSetPassword($user);

        $this->userRepository->save($user);
    }

    /**
     * @param User $user
     */
    public function encodeAndSetPassword(User $user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
    }

    /**
     * @param User $user
     * @param bool $isEdit
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createUser(User $user, bool $isEdit)
    {
        if (!$isEdit) {
            $this->encodeAndSetPassword($user);
        }
        $this->userRepository->save($user);
    }

    /**
     * @return User[]
     */
    public function getAll()
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param User $user
     * @throws ORMException
     */
    public function delete(User $user)
    {
        $this->userRepository->removeAndFlush($user);
    }

    public function count()
    {
        return $this->userRepository->count([]);
    }

    public function findOneByValidationToken($validationToken)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy([
            'validationToken' => $validationToken
        ]);

        return $user;
    }

    /**
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(User $user)
    {
        $this->userRepository->save($user);
    }
}