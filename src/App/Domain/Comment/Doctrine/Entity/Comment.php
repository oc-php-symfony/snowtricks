<?php

namespace App\Domain\Comment\Doctrine\Entity;

use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\User\Doctrine\Entity\User;
use DateTime;
use Exception;

class Comment
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Trick
     */
    private $trick;

    /**
     * @var DateTime $created
     *
     */
    protected $createdAt;

    /**
     * @var DateTime $updated
     *
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Comment
     */
    public function setContent(string $content): Comment
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param User|null $user
     *
     * @return Comment
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Run by doctrine lifecycle callbacks on pre-persist and pre-update
     *
     * @throws Exception
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new DateTime();

        $this->setUpdatedAt($dateTimeNow);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function getCreatedAt() :?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt() :?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param User|null $user
     *
     * @return Comment
     */
    public function setTrick(Trick $trick = null)
    {
        $this->trick = $trick;

        return $this;
    }

    /**
     * @return Trick|null
     */
    public function getTrick()
    {
        return $this->trick;
    }

}