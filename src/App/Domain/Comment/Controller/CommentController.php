<?php

namespace App\Domain\Comment\Controller;

use App\Domain\Comment\Manager\CommentManager;
use App\Domain\Trick\Doctrine\Entity\Trick;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    public function loadComments(Trick $trick, int $page = 1)
    {
        /** @var CommentManager $commentManager */
        $commentManager = $this->get('app.comment.manager');
        $comments = $commentManager->loadTrickComments($trick, $page);

        $moreResults = $trick->getComments()->count() > intval($this->container->getParameter('app.comment.comment_per_page')) * $page;

        return $this->render('Comment\grid.html.twig', [
            'comments' => $comments,
            'moreResults' => $moreResults
        ]);
    }
}